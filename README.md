# Amtrak SMS Updates

## Summary

We've all been on a train with spotty data coverage, but adequate SMS.
Okay, maybe not all, but if you haven't tried it, you should! Anyway,
here's a crudely-built tool to send an automated SMS of the status to
a friend asking about their train without the cell service to fetch it
themselves.

## Usage

```
Usage: amtrak_status.py [OPTIONS]

Options:
  --route TEXT            [required]
  --destination TEXT
  --from-address TEXT
  --to-address TEXT
  --password TEXT
  --time-between MINUTES
  --repeat / --no-repeat
  --help                  Show this message and exit.
```

Amtrak routes are numbered, followed by a dash for the departure date,
for instance train 5-3 is the California Zephyr (5) that departed its
origin station (Chicago) on the 3rd.

If I were polishing that, I'd make better options there.

For email-to-SMS, you'd need to know the recipient's carrier. For
example Google Fi uses `<10-digit-number>@msg.fi.google.com`. There
are web resources to help you look those up.

The `--from-account` you use also has to be set up for SMTP, because
that's what I crib'd. It is also hard-coded to Gmail for now.

## Bugs

There's probably bugs e'rrywhere. I didn't test stuff.

Pull requests accepted.

## APIs

I'm using [Amtraker's API](https://amtraker.com/about)

Amtraker says the following:
> Can I use your data?
> Yes! Amtraker's data is available for use for free, as long as you don't spam the API ofc

Please respect their API and don't use this script to spam it.

## Final thoughts

This was thrown together in less than an hour. PRs accepted.
