import requests
from dataclasses import dataclass
from datetime import datetime
from dateutil import parser
import click
import smtplib
from subprocess import Popen, PIPE
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
from pytz import utc


@dataclass
class Station:
    name: str
    code: str
    tz: str
    bus: bool
    schArr: str
    schDep: str
    arr: str
    dep: str
    arrCmnt: str
    depCmnt: str
    status: str

    def __post_init__(self):
        self.schArr = parser.parse(self.schArr)
        self.schDep = parser.parse(self.schDep)
        self.arr = parser.parse(self.arr)
        self.dep = parser.parse(self.dep)


@click.command()
@click.option('--route', required=True)
@click.option('--destination')
@click.option('--from-address')
@click.option('--to-address')
@click.option('--password')
@click.option('--time-between', metavar='MINUTES', default=30)
@click.option('--repeat/--no-repeat', default=True)
def main(route, destination, from_address, to_address, password, time_between, repeat):
    try:
        if repeat:
            while True:
                send_message(route, destination, from_address, to_address, password)
                time.sleep(time_between * 60)
        else:
            send_message(route, destination, from_address, to_address, password)
    except StopIteration:
        print("Train has arrived!")

def send_message(route, destination, from_address, to_address, password):
    train_info = requests.get('https://api-v3.amtraker.com/v3/trains/{route}'.format(route=route)).json()
    train_number = route.split('-')[0]
    stations_list = train_info[train_number][0]['stations']
    stations = [Station(**station) for station in stations_list]
    i = 1
    try:
        while i < len(stations):
            if stations[i].status == 'Departed':
                del stations[i-1]
            else:
                i += 1
    except IndexError:
        pass
    if stations[-1].arr <= datetime.utcnow().replace(tzinfo=utc):
        raise StopIteration
    msg_body = []
    msg_body.append(STATUS_HEAD.format(a=stations[0]))
    for i in range(1, min(3, len(stations) - 2) + 1):
        msg_body.append(STATUS_BODY.format(a=stations[i]))
    if len(stations) > 5:
        msg_body.append('...')
    msg_body.append(STATUS_FOOT.format(a=stations[-1]))

    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'Amtrak ' + route
    msg['From'] = from_address
    msg['To'] = to_address

    body = '\n'.join(msg_body)
    print(body)
    msg.attach(MIMEText(body, 'plain'))

    serv = smtplib.SMTP('smtp.gmail.com:587')
    serv.starttls()
    serv.login(from_address.split('@')[0], password)
    serv.sendmail(from_address, to_address, msg.as_string())
    serv.quit()


STATUS_HEAD = '''
{a.status} {a.name} {a.depCmnt}
____
|DD|____T_
|_ |_____|<
  @-@-@-oo\\
'''
STATUS_BODY = '{a.status} {a.name} {a.depCmnt} at {a.schArr}'
STATUS_FOOT = '{a.status} {a.name} {a.depCmnt} at {a.schArr}'

if __name__ == '__main__':
    main()
